### 初始化子模块
    git submodule init
### 更新子模块
    git submodule update
### 切换分支
    git checkout mybranch
### 创建分支
    git checkout -b remotebranch
### 删除本地分支
    git branch -D localbranch
### 更新分支
    git pull
