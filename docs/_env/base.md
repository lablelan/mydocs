# 搭建后端环境(windows)

1. 安装必备环境(svn)
    * git
    * node 
    * mongodb 
    * redis 
2. 获取项目权限
    * 注册[gitlab](https://gitlab.qiukuixinxi.com)账号
    * 使用git生成公钥密钥
    * 将公钥给后端负责人
3. 使用开发分支([git](/git/base.md))
    * 使用git拉取[项目](https://gitlab.qiukuixinxi.com)并确保有所有子模块权限
    * 初始化并更新子模块
    * 切换到开发分支
        * server => develop
        * src => master
        * resource => svn
4. 配置开发环境
    * 从svn上获取后端配置(master.json,config.json,servers.json)并将配置放到server/config下
    * 手动在server目录下创建日志文件夹logs
    * 在项目根目录执行 npm install 安装依赖库
    * 启动redis
    * 在项目根目录执行 npm start 启动后端